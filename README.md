# Ubuntu 18.04 Open SSH Docker Image

### Run SSH Server Inside Your Docker Container 


#### How to pull the docker images?
``` docker pull pujianto/ubuntu-openssh```

#### How to run it
``` docker run -it -e "ROOT_PASSWORD=your_root_password" -p 2022:22 pujianto/ubuntu-openssh ```

Then access the container with `ssh root@localhost -p 2022`. I think this is a bit useful to simulate ansible remote, etc. 

